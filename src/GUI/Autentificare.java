package GUI;

import Code.Client;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import GUI.Menu;

public class Autentificare {
    private JPanel Fundal;
    private JPanel Bottom;
    private JPanel LeftPanel;
    private JPanel RightPanel;
    private JButton CancelButton;
    private JButton OkButton;
    private JTextField EmailInput;
    private JPasswordField PasswordInput;
    private JLabel EmailLabel;
    private JLabel PasswordLabel;
    private JRadioButton isAdminRadio;

    public Autentificare(){
        JFrame frame = new JFrame("Autentificare");
        frame.setContentPane(Fundal);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setResizable(false);
        OkButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                String email = EmailInput.getText();
                String parola = String.valueOf(PasswordInput.getPassword());

                if (isAdminRadio.isSelected()) {
                    Client client = new Client();
                    String query = new String();
                    query = "SELECT email, parola FROM users WHERE email = '" + email + "' AND parola = '" + parola + "' AND isAdmin = '" + 1 + "';";
                    try {
                        if (client.connectServer(3, query).equals("Ai fost logat ca MANAGER!")) {
                            frame.dispose();
                            Menu newMenu = new Menu();
                            newMenu.setVisibility(true);
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    Client client = new Client();
                    String query = new String();
                    query = "SELECT email, parola FROM users WHERE email = '" + email + "' AND parola = '" + parola + "';";
                    try {
                        if (client.connectServer(1, query).equals("Ai fost logat!")) {
                            frame.dispose();
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }

            }
        });
        CancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
    }
}
