package GUI;

import org.omg.CORBA.PRIVATE_MEMBER;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Menu {
    private JButton login;
    private JButton autentificare;
    private JPanel Containter;
    private JButton manager;

    public void setVisibility(boolean value)
    {
        manager.setVisible(value);
    }
    public Menu(){
        JFrame frame = new JFrame("Menu");
        frame.setContentPane(Containter);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(300,150);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
        manager.setVisible(false);
        autentificare.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Inregistrare inreg = new Inregistrare();
            }
        });
        login.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Autentificare auth = new Autentificare();
            }
        });
        manager.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                AdminMenu am = new AdminMenu();
            }
        });
    }


}
