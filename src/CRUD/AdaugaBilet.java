package CRUD;

import Code.Bilet;
import Code.Client;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class AdaugaBilet {
    private JPanel Container;
    private JButton cancelButton;
    private JButton submitButton;
    private JTextField numarBiletInput;
    private JTextField idSalaCinema;
    private JTextField idCumparator;

    public AdaugaBilet(){
        JFrame frame = new JFrame("AdaugareBilet");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);

        submitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String numar = numarBiletInput.getText();
                Integer n = Integer.parseInt(numar);
                String sala = idSalaCinema.getText();
                Integer s = Integer.parseInt(sala);
                String user = idCumparator.getText();
                Integer u = Integer.parseInt(user);

                Bilet ticket = new Bilet(n,s,true,u);
                String query = "INSERT INTO tickets (id_sala, isOcupat, id_cumparator, numarBilet) VALUES (" + ticket.toString()+");";
                int tag = 4;
                //apelez Clientul
                Client cl = new Client();
                try {
                    if (cl.connectServer(tag, query).equals("Bilet nou adaugat!")){
                        frame.dispose();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
    }
}
