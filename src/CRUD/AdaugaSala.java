package CRUD;

import Code.SalaCinema;
import Code.Client;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class AdaugaSala {
    private JTextField numeSalaInput;
    private JTextField numarLocuriInput;
    private JTextField managerInput;
    private JTextField oraDeschidereInput;
    private JTextField oraInchidereInput;
    private JButton submitButton;
    private JButton cancelButton;
    private JPanel Container;

    public AdaugaSala() {
        JFrame frame = new JFrame("AdaugareSala");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
        submitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String nume = numeSalaInput.getText();
                String numarLocuri = numarLocuriInput.getText();
                Integer nr = Integer.parseInt(numarLocuri);
                String manager = managerInput.getText();
                int m = Integer.parseInt(manager);
                String open = oraDeschidereInput.getText();
                int o = Integer.parseInt(open);
                String close = oraInchidereInput.getText();
                int c = Integer.parseInt(close);

                SalaCinema sala = new SalaCinema(nume,nr,m,o,c);
                String query = "INSERT INTO salicinema (numeSala, locuriDisponibile, oraStart, oraEnd, id_manager) VALUES (" + sala.toString() + ");";
                int tag = 8;
                //apelez Clientul
                Client cl = new Client();
                try {
                    if (cl.connectServer(tag, query).equals("Sala noua creata!")){
                        frame.dispose();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        });
        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
    }
}
