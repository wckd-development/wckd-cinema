package CRUD;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import Code.Client;

public class ModificaSala {
    private JPanel Container;
    private JButton submitButton;
    private JButton cancelButton;
    private JTextField idSalaInput;
    private JTextField numeSalaInput;
    private JTextField numarLocuriInput;
    private JTextField managerSalaInput;
    private JTextField oraStartInput;
    private JTextField oraEndInput;

    public ModificaSala() {

        JFrame frame = new JFrame("ModificaSala");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);

        submitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String numar = idSalaInput.getText();
                int s = Integer.parseInt(numar);
                String nume = numeSalaInput.getText();
                String numarLocuri = numarLocuriInput.getText();
                int nr = Integer.parseInt(numarLocuri);
                String manager = managerSalaInput.getText();
                int m = Integer.parseInt(manager);
                String open = oraStartInput.getText();
                int o = Integer.parseInt(open);
                String close = oraEndInput.getText();
                int c = Integer.parseInt(close);
                String query = "UPDATE salicinema SET numesala = '" + nume + "', " + "locuriDisponibile = " + nr + " ,oraStart = " + o + ", "  + "oraEnd = " + c + ", id_manager = " + m + " WHERE idsaliCinema = " + s +";";
                int tag = 9;
                //apelez Clientul
                Client cl = new Client();
                try {
                    if (cl.connectServer(tag, query).equals("Sala modificata!")){
                        frame.dispose();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
    }
}
