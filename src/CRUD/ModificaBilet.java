package CRUD;

import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;

import Code.Client;

public class ModificaBilet {
    private JPanel Container;
    private JButton cancelButton;
    private JButton submitButton;
    private JTextField numarBilet;
    private JTextField idSalaDeCinema;
    private JTextField idCumparator;
    private JCheckBox achizitionatCheckBox;
    private JTextField numarBiletInput;
    private JPanel panel1;
    private JRadioButton Achizitionat;
    private Boolean ocupat = false;

    public ModificaBilet() {
        JFrame frame = new JFrame("ModificaBilet");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);

        submitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String numar = numarBilet.getText();
                int n = Integer.parseInt(numar);
                String sala = idSalaDeCinema.getText();
                int s = Integer.parseInt(sala);
                String user = idCumparator.getText();
                int u = Integer.parseInt(user);
                String aux = numarBiletInput.getText();

                int tag = 5;
                String query;
                query = "UPDATE tickets SET id_sala =" + s + ", isOcupat =" + ocupat +", id_cumparator = " + u + ", numarBilet = " + aux + " WHERE idtickets =" + n +";" ;

                Client cl = new Client();
                try {
                    if (cl.connectServer(tag, query).equals("Bilet modificat!")){
                        frame.dispose();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });

        achizitionatCheckBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.DESELECTED)
                    ocupat = false;
                else ocupat = true;
            }
        });
    }
}
