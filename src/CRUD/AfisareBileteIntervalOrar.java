package CRUD;

import Code.Client;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class AfisareBileteIntervalOrar {
    private JPanel Container;
    private JButton afiseazaButton;
    private JButton cancelButton;
    private JTextField oraStartInput;
    private JTextField oraEndInput;
    private JPanel panel1;

    public AfisareBileteIntervalOrar() {

        JFrame frame = new JFrame("AfiseazaBilete");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);

        afiseazaButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String open = oraStartInput.getText();
                String close = oraEndInput.getText();
                int o = Integer.parseInt(open);
                int c = Integer.parseInt(close);
                String query;
                query = "SELECT * FROM tickets t JOIN salicinema s ON (t.id_sala = s.idsaliCinema) WHERE s.oraStart >= " + o + " AND s.oraEnd <= " + c + ";";
                Client cl = new Client();
                int tag = 10;
                try {
                    if (cl.connectServer(tag, query).equals("Biletul a fost sters!")){
                        frame.dispose();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        });
        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
    }
}
