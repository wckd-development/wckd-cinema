package Code;

public class Utilizator {
    private String nume;
    private String prenume;
    private String email;
    private String parola;
    private String mesajDeEroare = new String();
    private boolean existaEroare = false;
    private boolean isAdmin = false;
    public String getMesajDeEroare() {
        return mesajDeEroare;
    }

    // Gettere pentru campurile private
    public String getNume() {
        return nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public String getEmail() {
        return email;
    }

    public String getParola() {
        return parola;
    }

    ///Functii de validare a datelor
    private boolean validareParola(String password, String confirm) {
        return password.equals(confirm) && password.matches(".+");
    }

    private boolean validareNume(String nume) {
        return nume.matches("[a-zA-Z]*.+");
    }

    private boolean validarePrenume(String prenume) {
        return prenume.matches("[a-zA-Z]*.+");
    }

    private boolean validareEmail(String email) {
        return email.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$");
    }

    public boolean getExistaEroare() {
        return existaEroare;
    }
    ///Constructor
    public Utilizator(String surname, String firstname, String email, String password, String confirm) {
        String eroareNume;
        String eroarePrenume;
        String eroareEmail;
        String eroareParola;
        if (validareNume(surname))
            this.nume = surname;
        else {
            eroareNume = "Nume invalid.";
            mesajDeEroare += eroareNume;
            mesajDeEroare += "\n";
            existaEroare = true;
        }
        if (validarePrenume(firstname))
            this.prenume = firstname;
        else {
            eroarePrenume = "Prenume invalid.";
            mesajDeEroare += eroarePrenume;
            mesajDeEroare += "\n";
            existaEroare = true;
        }
        if (validareEmail(email))
            this.email = email;
        else {
            eroareEmail = "Email invalid.";
            mesajDeEroare += eroareEmail;
            mesajDeEroare += "\n";
            existaEroare = true;
        }
        if (validareParola(password, confirm))
            this.parola = password;
        else {
            eroareParola = "Parola nu a fost validata.";
            mesajDeEroare += eroareParola;
            mesajDeEroare += "\n";
            existaEroare = true;
        }
    }

}
