package Code;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;

class Server {
    public static void main(String[] sir) throws IOException, SQLException, ClassNotFoundException {
        String newline = System.getProperty("line.separator");
        ServerSocket ss;
        Socket cs;
        ss = new ServerSocket(8000);
        System.out.println("Serverul a pornit !");

        ///LOAD DATABASE

        Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "wckd");

        /// INCOMING QUERY FROM CLIENT

        int i = 0;
        while (i < 1000) { /// 1000 de conexiuni
            cs = ss.accept(); ///ASTEPTAM CONEXIUNE CU CLIENTUL
            i++;

            System.out.println("Conexiune efectuata");

            ObjectInputStream dis;
            DataOutputStream dos;
            dis = new ObjectInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());

            Stream stream;
            stream = (Stream) dis.readObject(); /// QUERY RECEPTIONAT
            String SQLQuery = stream.SQL;
            SQLQuery = SQLQuery.trim();

            /// CRUD DATABASE

            String raspunsCatreClient = "";

            ///LOGIN

            if (stream.code == 1) {
                Statement myState = myConn.createStatement();
                ResultSet myRs = myState.executeQuery(SQLQuery);
                int count = 0;
                while (myRs.next()) {
                    count++;
                }
                if (count != 0) raspunsCatreClient = "Ai fost logat!";
                else raspunsCatreClient = "User/Parola gresite!";
            }

            ///REGISTER

            else if (stream.code == 2) {
                Statement myState = myConn.createStatement();
                int myRs = myState.executeUpdate(SQLQuery);
                raspunsCatreClient = "Cont creat!";
            }

            ///ADMIN LOGIN

            else if (stream.code == 3) {
                Statement myState = myConn.createStatement();
                ResultSet myRs = myState.executeQuery(SQLQuery);
                int count = 0;
                while (myRs.next()) {
                    count++;
                }
                if (count != 0) raspunsCatreClient = "Ai fost logat ca MANAGER!";
                else raspunsCatreClient = "User/Parola gresite sau NU ESTI MANAGER!";
            }

            ///ADAUGA BILETE
            else if (stream.code == 4) {
                Statement myState = myConn.createStatement();
                int myRs = myState.executeUpdate(SQLQuery);
                raspunsCatreClient = "Bilet nou adaugat!";
            }

            ///MODIFICA BILETE
            else if (stream.code == 5) {
                Statement myState = myConn.createStatement();
                int myRs = myState.executeUpdate(SQLQuery);
                raspunsCatreClient = "Bilet modificat!";
            }

            ///VIZUALIZEAZA BILETE
            else if (stream.code == 6) {
                Statement myState = myConn.createStatement();
                ResultSet myRs = myState.executeQuery(SQLQuery);
                while (myRs.next()) {
                    raspunsCatreClient += "ID Ticket: " + myRs.getString("idtickets") + "| ";
                    raspunsCatreClient += "ID Sala: " + myRs.getString("id_sala") + "| ";
                    raspunsCatreClient += "Achizitionat: " + myRs.getString("isOcupat") + "| ";
                    raspunsCatreClient += "Id_cumparator: " + myRs.getString("id_cumparator") + "| ";
                    raspunsCatreClient += "Numar Bilet: " + myRs.getString("numarBilet") + newline;
                }
            }
            ///STERGE BILETE
            else if (stream.code == 7) {
                Statement myState = myConn.createStatement();
                int myRs = myState.executeUpdate(SQLQuery);
                raspunsCatreClient = "Biletul a fost sters!";
            }
            ///ADAUGA SALA
            else if (stream.code == 8) {
                Statement myState = myConn.createStatement();
                int myRs = myState.executeUpdate(SQLQuery);
                raspunsCatreClient = "Sala noua creata!";
            }
            ///MODIFICA SALA
            else if (stream.code == 9) {
                Statement myState = myConn.createStatement();
                int myRs = myState.executeUpdate(SQLQuery);
                raspunsCatreClient = "Sala modificata!";
            }

            /// AFISARE BILETE ORE

            else if (stream.code == 10) {
                Statement myState = myConn.createStatement();
                ResultSet myRs = myState.executeQuery(SQLQuery);
                while (myRs.next()) {
                    raspunsCatreClient += "ID Ticket:  " + myRs.getString("idtickets") + "| ";
                    raspunsCatreClient += "ID Sala: " + myRs.getString("id_sala") + "| ";
                    raspunsCatreClient += "Achizitionat: " + myRs.getString("isOcupat") + "| ";
                    raspunsCatreClient += "Id_cumparator: " + myRs.getString("id_cumparator") + "| ";
                    raspunsCatreClient += "Numar Bilet: " + myRs.getString("numarBilet") + newline;
                }
            }

            /// AFISARE BILETE DINTR-O SALA

            else if (stream.code == 11) {
                Statement myState = myConn.createStatement();
                ResultSet myRs = myState.executeQuery(SQLQuery);
                while (myRs.next()) {
                    raspunsCatreClient += "ID Ticket: " + myRs.getString("idtickets") + "| ";
                    raspunsCatreClient += "ID Sala:  " + myRs.getString("id_sala") + "| ";
                    raspunsCatreClient += "Achizitionat: " + myRs.getString("isOcupat") + "| ";
                    raspunsCatreClient += "Id_cumparator: " + myRs.getString("id_cumparator") + "| ";
                    raspunsCatreClient += "Numar Bilet: " + myRs.getString("numarBilet") + newline;
                }
            }


            /// RASPUNS CATRE CLIENT

            dos.writeUTF(raspunsCatreClient);
            cs.close();
            dis.close();
            dos.close();
        }

    }
}

