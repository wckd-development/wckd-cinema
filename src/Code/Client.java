package Code;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javax.swing.*;

public class Client{

    public String connectServer(int code, String SQL) throws IOException {

        ///CONEXIUNE LA SERVER

        Socket cs = null;
        try {
            cs = new Socket("localhost", 8000);
        } catch (Exception e) {
            System.out.println("Conexiune esuata");
            System.exit(1);
        }

        ///IN & OUT CLIENT-SERVER

        DataInputStream dis;
        ObjectOutputStream dos;
        dis = new DataInputStream(cs.getInputStream());
        dos = new ObjectOutputStream(cs.getOutputStream());

        /// TRIMITEM QUERY SI ASTEPTAM RASPUNS DE LA SERVER

        System.out.println("Se trimite QUERY-ul SQL \t");
        System.out.println(SQL);
        SQL = SQL.trim();
        Stream stream = new Stream(code,SQL);
        dos.writeObject(stream); ///QUERY SE TRIMITE LA SERVER

        ///SERVERUL SE CONECTEAZA LA BAZA DE DATE SI RETURNEAZA UN STRING

        String raspunsDeLaServer = dis.readUTF();
        JOptionPane.showMessageDialog(null, raspunsDeLaServer);
        System.out.println("CONEXIUNE CLIENT TERMINATA!");

        return raspunsDeLaServer;
    }
}

