package Code;

public class Bilet {
    private int numarBilet;
    private int idSala;
    private Boolean isOcupat;
    private int idCumparator;

    public Bilet(int bilet, int sala, Boolean ocupat, int user){
        this.numarBilet = bilet;
        this.idSala = sala;
        this.isOcupat = ocupat;
        this.idCumparator = user;
    }

    @Override
    public String toString() {
        String sir;
        sir = Integer.toString(idSala);
        sir = sir + ',';
        if(isOcupat)
            sir = sir + Integer.toString(1);
        else
            sir = sir + Integer.toString(0);
        sir = sir + ',';
        sir = sir + Integer.toString(idCumparator);
        sir = sir + ',';
        sir = sir + numarBilet;
        return sir;
    }
}
